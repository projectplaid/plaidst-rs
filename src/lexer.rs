#[derive(PartialEq, Debug)]
pub enum Token {
    EOF,
    Invalid,
    Identifier(Vec<char>),
    Char(char),
    Number(Vec<char>),
    Semicolon(char),
    Dot(char),
    LBrace(char),
    RBrace(char),
    LParen(char),
    RParen(char),
    LBracket(char),
    RBracket(char),
}

pub struct Lexer {
    input: Vec<char>,

    pub position: usize,
    pub read_position: usize,
    pub ch: char,
}

impl Lexer {
    pub fn new(input: Vec<char>) -> Self {
        Self {
            input,
            position: 0,
            read_position: 0,
            ch: '0',
        }
    }

    pub fn read_char(&mut self) {
        if self.read_position >= self.input.len() {
            self.ch = '0';
        } else {
            self.ch = self.input[self.read_position];
        }
        self.position = self.read_position;
        self.read_position = self.read_position + 1;
    }

    pub fn skip_whitespace(&mut self) {
        let ch = self.ch;
        if ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r' {
            self.read_char();
        }
    }

    pub fn next_token(&mut self) -> Token {
        let tok: Token = Token::Identifier(vec![' ']);

        match self.ch {
            /*
            '=' => {
                tok = Token::ASSIGN(self.ch);
            },
            '+' => {
                tok = Token::PLUS(self.ch);
            },
            '-' => {
                tok = Token::MINUS(self.ch);
            },
            '!' => {
                tok = Token::BANG(self.ch);
            },
            Other patterns
            */
            _ => {}
        }
        self.read_char();
        tok
    }
}
